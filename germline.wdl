workflow germline {
  File ref_fasta
  File ref_fasta_index
  File sample_name
  
  call bwa_mem_tool {
    input:
      ref_fasta = ref_fasta,
      ref_fasta_index = ref_fasta_index,
      sample = sample_name
  }

  call samtools_2bam {
    input:
      input_sam = bwa_mem_tool.output_sam,
      sample = sample_name
  }
}

task bwa_mem_tool {
  String sample
  File reads1
  File reads2
  Int min_seed_length
  Int threads
  File ref_fasta
  File ref_fasta_index

  command {
    bwa mem  \
      -t ${threads} \
      -k ${min_seed_length} \
    ${ref_fasta} \
    ${reads1} ${reads2} > "${sample}.sam"
  }

  output {
    File output_sam = "${sample}.sam"
  }
}

task samtools_2bam {
  String sample
  File input_sam

  command {
    samtools view \
      -bS ${input_sam} > "${sample}.bam"
  }

  output {
    File output_bam = "${sample}.bam"
  }
}
